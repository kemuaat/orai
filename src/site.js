"use strict";
class Site {
    constructor(time, text) {
        this.num = time;
        this.name = text;
    }
    greetings() {
        return 'Hello from ' + this.name;
    }
    promiseMe() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (this.num < 10)
                    reject("A szam tul kicsi");
                resolve("Ez a szam pont jo");
            }, 3000);
        });
    }
}
exports.Site = Site;
//# sourceMappingURL=site.js.map