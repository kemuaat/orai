"use strict";
const site_1 = require("./site");
var site;
document.getElementById("messageArea").innerHTML = 'Halo';
function changeNumField(msg) {
    document.getElementById("numberField").innerHTML = msg;
}
module.exports = {
    createSite: function () {
        console.log("creating the Site");
        site = new site_1.Site(parseInt(document.getElementById("ageField").value), document.getElementById("nameField").value);
    },
    getMessage: function () {
        document.getElementById("messageArea").innerHTML = site.greetings();
        site.promiseMe().then(data => {
            changeNumField(data);
        }).catch(error => {
            changeNumField(error);
        });
        changeNumField("-");
    }
};
//# sourceMappingURL=main.js.map