"use strict";
class Human {
    constructor(num, text) {
        this.age = num;
        this.name = text;
    }
    greeting() {
        return 'hello from ' + this.name;
    }
}
exports.Human = Human;
//# sourceMappingURL=human.js.map